#include<stdio.h>
#include<math.h>
 
main(){
	float xatas=0,xbawah=-1, x, fxatas, fxbawah, fx;
	int n=10;
	double e=0.01;

	printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	printf("\n");
	printf("|    A    |    B    |    X    |   F(a)   |   F(b)   |   F(x)   |");
	printf("\n");
	printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	printf("\n");
			
	//loop sesuai n
	for(int a=0;a<n;a++){
		fxatas=(float)xatas*exp(-xatas)+1;//hitung fa
		fxbawah=(float)xbawah*exp(-xbawah)+1;//hitung fb
		
		x=(xatas+xbawah)/2;//hitung nilai x
		
		fx=x*exp(-x)+1;//hitung nilai fx

		//jika fa di kali fb lebih dari 0
		if(((float)fxatas*(float)fxbawah)>0){
			break;//berhenti
		}else{
			//lanjut
			if((float)(fx*fxatas)>0){//Jika fx dikalikan fa hasilnya lebih dari 0 maka
				xatas=x;
				xbawah=xbawah;
			}else{//jika tidak
				xbawah=x;
				xatas=xatas;
			}
			printf("| %4.5lf| %4.5lf| %4.5lf| %4.5lf  | %4.5lf  |", xbawah, xatas, x,fxatas, fxbawah);
			//printf("a=>%.5lf,b=>%.5lf,x=>%.5lf,fa=>%.5lf,fb=>%.5lf,fx=>%.5lf", xbawah,xatas, x, fxatas, fxbawah, fx);
			printf("\n");
		}
	}
	printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	printf("\n\n");
}